//
//  Loader.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 12/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import "Loader.h"
#import "Helpers.h"

@interface Loader()

@property (nonatomic, strong) NSURL *URL;

@end

@implementation Loader

- (instancetype)init {

    self = [super init];

    if (self) {

        _writer.parserStructure = [ParserStructure new];
        _writer.parserEvent = [ParserEvent new];
    }

    return self;
}

- (void)loadURL:(NSURL *)URL {

    self.URL = URL;

    if (![self isHTTPURL:URL]) {
        [self reportError:[Helpers errorFromObject:self withReason:@"not a valid URL"]];
        return;
    }

        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {

            if (![response isKindOfClass:[NSHTTPURLResponse class]]) {

                [self reportError:connectionError ? : [Helpers errorFromObject:self withReason:@"not an HTTPResponse"]];
                return ;
            }

            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (![self isSuccessfulHTTPResponse:httpResponse]) {
                [self reportError:connectionError];
                return;
            }

            if (data) {

                [self.writer writeData:data];
            }
        }];
}


- (BOOL)isHTTPURL:(NSURL *)url {

    NSString *scheme = [[url scheme] lowercaseString];
    return [scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"];
}

- (BOOL)isSuccessfulHTTPResponse:(NSHTTPURLResponse *)response {

    return response.statusCode >= 200 && response.statusCode < 300;
}

- (void)reportError:(NSError *)error {

    NSLog(@"error: %@", error);
    [self.delegate loader:self didLoadURL:self.URL];
}

@end
