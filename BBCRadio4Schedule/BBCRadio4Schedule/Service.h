//
//  Service.h
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 12/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Loader.h"
#import "Writer.h"

@interface Service : NSObject <LoaderDelegate>

- (void)load;

@property (nonatomic, strong, nonnull) NSManagedObjectContext *managedObjectContext;

@end
