//
//  Loader.h
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 12/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSURLConnection.h>
#import "Writer.h"

@class Loader;

@protocol LoaderDelegate <NSObject>

- (void)loader:(Loader *)loader didLoadURL:(NSURL *)URL;

@end

@interface Loader : NSObject

@property (nonatomic, weak) id <LoaderDelegate> delegate;
@property (nonatomic, strong) Writer *writer;

- (void)loadURL:(NSURL *)URL;

@end
