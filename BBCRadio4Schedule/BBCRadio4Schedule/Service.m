//
//  Service.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 12/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import "Service.h"

@interface Service ()

@property (nonatomic, strong) NSMutableArray *loaders;

@end

@implementation Service

- (instancetype)init {
    self = [super init];
    if (self) {
        _loaders = [NSMutableArray new];
    }
    return self;
}

- (void)load {
    
    NSBundle *bundle = [NSBundle bundleForClass:self.class];
    NSString *filePath = [bundle pathForResource:@"Service" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSArray *toLoad = [NSArray arrayWithArray:[dict objectForKey:@"Root"]];

    for (NSString *urlString in toLoad) {
        NSURL *URL = [NSURL URLWithString:urlString];
        Loader *loader = [Loader new];
        loader.writer = [Writer new];
        loader.writer.managedObjectContext = self.managedObjectContext;

        [self.loaders addObject:loader];
        [loader loadURL:URL];

    }
}

- (void)loader:(Loader *)loader didLoadURL:(NSURL *)URL {
    [self.loaders removeObject:loader];
}

@end
