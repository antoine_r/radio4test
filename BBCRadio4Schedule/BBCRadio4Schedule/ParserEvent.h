//
//  ParserEvent.h
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParserFacade.h"

@interface ParserEvent : NSObject <ParserFacade>

@end
