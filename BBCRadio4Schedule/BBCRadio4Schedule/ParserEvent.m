//
//  ParserEvent.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import "ParserEvent.h"

@implementation ParserEvent

- (BOOL)isValid:(id)object { // I have to timebox this exercise. the idea demonstrated here is that each element accessed later should be evaluated here. If mandatory return invalid if not present. if the item is present, check it's type and when relevent if the value is valid. i.e. a date might need to conform to the expected format and a number might be within a range.

    if (![object isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    NSDictionary *event = (NSDictionary *)object;

    // title
    if (!event[@"display_titles"]) {
        return NO;
    }

    if (![self isTitleValidInEvent:event]) {
        return NO;
    }

    return YES;
}

- (BOOL)isShortSynopsisValidInEvent:(NSDictionary *)event {

    return YES;
}

- (BOOL)isTitleValidInEvent:(NSDictionary *)event {

    NSDictionary *displaytitleDictionary = event[@"display_titles"];
    NSString *title = displaytitleDictionary[@"title"];

    if (!title || ![title isKindOfClass:[NSString class]]) {
        return NO;
    }

    return YES;
}

@end
