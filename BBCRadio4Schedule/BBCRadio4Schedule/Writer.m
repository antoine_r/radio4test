//
//  Writer.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import "Writer.h"

@implementation Writer

- (void)writeData:(NSData *)data {

    if (![self.parserStructure isValid:data]) { // NB: I validate the integrigrity of the structure so I do not have to write code defensively in the Writer class.
        return ;
    }

    NSDictionary *root = (NSDictionary *)data;

    NSDictionary *day = root[@"day"];

    NSDate *dateBroadcasted = [self dateBroadcastedFromDayDictionary:day];

    for (NSDictionary *anEvent in day) {

        if (![self.parserEvent isValid:anEvent]) { // NB: I validate the integrigrity of the element so I do not have to write code defensively in the Writer class.

            continue;
        }

        [self createOrUpdateEventForEventDictionary:anEvent dayBroadcasted:dateBroadcasted];
    }
    
    BOOL saved = [self.managedObjectContext save:NULL];
    if (!saved) {
        [self.managedObjectContext reset];
    }
}

- (NSDate *)dateBroadcastedFromDayDictionary:(NSDictionary *)dayDictionary {

    NSString *dateString = dayDictionary[@"date"];

    return [self dateWithFormatString:@"yyyy-MM-dd" forDateString:dateString];
}

- (NSDate *)dateWithFormatString:(NSString *)format forDateString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

- (Event *)createOrUpdateEventForEventDictionary:(NSDictionary *)eventDictoinary dayBroadcasted:(NSDate *)dayBroadcasted {

    NSDate *startDateTime = nil;
    Event *event = [self fetchEventWithPID:eventDictoinary[@""] startDateTime:startDateTime];
    if (!event) {
        event = [self newEventFromEventDictionary:eventDictoinary dayBroadcasted:dayBroadcasted];
    }
    return event;
}

- (Event *)newEventFromEventDictionary:(NSDictionary *)eventDictionary dayBroadcasted:(NSDate *) dayBroadcasted {
    Event *event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:self.managedObjectContext];

    event.pid = eventDictionary[@"pid"];
    event.dayBroadcasted = dayBroadcasted;
    event.pidPhoto = eventDictionary[@"programme"][@"image"][@"pid"];
    event.title = eventDictionary[@"display_titles"][@"title"];
    event.synopsis = eventDictionary[@"programme"][@"short_synopsis"];
    event.startDateTime = [self dateWithFormatString:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ" forDateString:eventDictionary[@"start"]];

    return event;
}

- (Event *)fetchEventWithPID:(NSString *)pid startDateTime:(NSDate *)startDateTime {

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    // Specify criteria for filtering which objects to fetch
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pid == %@", pid];
    [fetchRequest setPredicate:predicate];
    // Specify how the fetched objects should be sorted
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDateTime"
                                                                   ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];

    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if (fetchedObjects == nil || fetchedObjects.count == 0) {
        return nil;
    }

    return fetchedObjects[0];
}

@end
