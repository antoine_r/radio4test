//
//  Event+CoreDataProperties.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Event+CoreDataProperties.h"

@implementation Event (CoreDataProperties)

@dynamic pid;
@dynamic pidPhoto;
@dynamic startDateTime;
@dynamic dayBroadcasted;
@dynamic synopsis;
@dynamic title;

@end
