//
//  Event+CoreDataProperties.h
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Event.h"

NS_ASSUME_NONNULL_BEGIN

@interface Event (CoreDataProperties)

@property (nonatomic, strong) NSString *pid;
@property (nonatomic, strong) NSString *pidPhoto;
@property (nonatomic, strong) NSDate *startDateTime;
@property (nonatomic, strong) NSDate *dayBroadcasted;
@property (nonatomic, strong) NSString *synopsis;
@property (nonatomic, strong) NSString *title;

@end

NS_ASSUME_NONNULL_END
