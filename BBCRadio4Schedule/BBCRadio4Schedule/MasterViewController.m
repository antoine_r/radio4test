//
//  MasterViewController.m
//  BBCRadio4Schedule
//
//  Created by IG on 08/09/2015.
//  Copyright (c) 2015 Super Cool Start-up. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "R4CellDay.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@end

@implementation MasterViewController //TODO: extract the tableview/collectionview logic to an external library.

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    R4CellDay *cell = [tableView dequeueReusableCellWithIdentifier:@"IGGCellDay" forIndexPath:indexPath];
    cell.collectionView.delegate = self;
    cell.collectionView.dataSource = self;
    [cell.collectionView reloadData];
    


    return cell;
}

#pragma mark - Collection View

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Today";
            break;
        case 1:
            return @"Tomorrow";
            break;
        case 2:
            return @"Yesterday";
            break;
        default:
            return @"";
            break;
    }
}

- (NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"programmeCell" forIndexPath:indexPath];

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [self findCellWithCollectionView:collectionView inTableView:self.tableView];
    NSIndexPath *indexPathOfcell = [self.tableView indexPathForCell:cell];
    NSInteger section = indexPathOfcell.section;
    self.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:section];
    [self performSegueWithIdentifier:@"showDetail" sender:nil];
}

- (UITableViewCell *)findCellWithCollectionView:(UICollectionView *)collectionView inTableView:(UITableView *)tableView {

    NSArray <R4CellDay *> *visibleCells = [tableView visibleCells];

    for (R4CellDay *cell in visibleCells) {

        if (cell.collectionView == collectionView) {

            return cell;
        }
    }
    return nil;
}


@end
