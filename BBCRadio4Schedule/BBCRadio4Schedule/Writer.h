//
//  Writer.h
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParserStructure.h"
#import "ParserEvent.h"
#import "Event.h"
#import <CoreData/CoreData.h>

@interface Writer : NSObject

- (void)writeData:(nonnull NSData *)data;

@property (nonatomic, strong, nonnull) ParserStructure *parserStructure;
@property (nonatomic, strong, nonnull) ParserEvent *parserEvent;
@property (nonatomic, strong, nonnull) NSManagedObjectContext *managedObjectContext;

@end
