//
//  R4ProgrammeCollectionViewCell.h
//  radio4TestVC
//
//  Created by Antoine Rabanes on 12/06/2016.
//  Copyright © 2016 App Revolution Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGGCollectionViewCellEvent : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelForTitle;

@end
