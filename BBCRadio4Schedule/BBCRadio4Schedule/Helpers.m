//
//  Helpers.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import "Helpers.h"

@implementation Helpers

+ (NSError *)errorFromObject:(NSObject *)object withReason:(NSString *)reason {
    return [NSError errorWithDomain:[object description] code:42 userInfo:@{NSLocalizedDescriptionKey: reason ? : @""}];
}

@end
