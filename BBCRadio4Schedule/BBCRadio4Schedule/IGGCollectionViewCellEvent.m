//
//  R4ProgrammeCollectionViewCell.m
//  radio4TestVC
//
//  Created by Antoine Rabanes on 12/06/2016.
//  Copyright © 2016 App Revolution Ltd. All rights reserved.
//

#import "IGGCollectionViewCellEvent.h"

@implementation IGGCollectionViewCellEvent

- (void)layoutSubviews {
    [super layoutSubviews];

    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = self.bounds;
    gradientMask.colors = @[(id)[UIColor blackColor].CGColor,
                            (id)[UIColor clearColor].CGColor];
    gradientMask.locations = @[@0.75, @1.0];
    self.layer.mask = gradientMask;
}


@end
