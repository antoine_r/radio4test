//
//  R4ProgrammeCell.h
//  radio4TestVC
//
//  Created by Antoine Rabanes on 11/06/2016.
//  Copyright © 2016 App Revolution Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface R4CellDay : UITableViewCell

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;


@end
