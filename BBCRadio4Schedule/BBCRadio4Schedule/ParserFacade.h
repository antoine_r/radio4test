//
//  ParserFacade.h
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ParserFacade <NSObject>

- (BOOL)isValid:(id)object; // by leaving the concrete implementation of the parsers behind a facade I can swap the parsing without the objects relying on it being affected. This leads to a code that evolves painlessly.

@end