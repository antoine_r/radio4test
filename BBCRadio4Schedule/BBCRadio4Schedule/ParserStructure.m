//
//  Parser.m
//  BBCRadio4Schedule
//
//  Created by Antoine Rabanes on 13/06/2016.
//  Copyright © 2016 Super Cool Start-up. All rights reserved.
//

#import "ParserStructure.h"

@implementation ParserStructure

- (BOOL)isValid:(id)object {

    if (![object isKindOfClass:[NSData class]]) {
        return NO;
    }
    NSData *data = (NSData *)object;

    NSError *error = nil;
    id JSON = [NSJSONSerialization
               JSONObjectWithData:data
               options:0
               error:&error];

    if (!JSON) {
        return NO;
    }

    if (![JSON isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    NSDictionary *dictionary = (NSDictionary *)JSON;

    if (!dictionary[@"day"]) {
        return NO;
    }

    if ([self isDayBroadcastingDateValidInDictionary:dictionary]) {
        return NO;
    }

    NSArray *broadcast = dictionary[@"broadcasts"];

    if (!broadcast || ![broadcast isKindOfClass:[NSArray class]]) {
        return NO;
    }

    return YES;
}

- (BOOL)isDayBroadcastingDateValidInDictionary:(NSDictionary *)dictionary {
// NB: it is important for this date to be valid as we will use it to query for the event later.
    NSString *dateString = dictionary[@"day"][@"date"];
    if (!dateString) {
        return NO;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];

    if (!date) {
        return NO;
    }
    return YES;
}

@end
